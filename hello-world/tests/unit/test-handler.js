'use strict';
var event, context;
var chai = require('chai')
var chaiHttp = require('chai-http');
chai.use(chaiHttp);
const expect = chai.expect;
var app = 'localhost:3000';

describe("Unit Testing", function() {
    describe("Send API's testing", function() {
        this.timeout(15000);
        it("Call Post API with Json", (done)=> {
            // Send some Form Data
            this.timeout(10000)
             chai.request(app)
            .post('/hello')
            .send({
            message: 'Hello', 
            empId: '1111'
            })
            .end(function (err, res) {
                if(err) done(err);
                expect(res.body.message).to.equal('Ok')
                done()
            })
        });
        it("Call Post API with without sending JSON", (done)=> {
            // Send some Form Data
            this.timeout(10000)
            chai.request(app)
            .post('/hello')
            .send()
            .end(function (err, res) {
                expect(res.body.message).to.equal('Json not included in the body');
                done()
            })
        });

    });
});
