// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */

const AWS = require('aws-sdk');
const S3=new AWS.S3();
const Bucket='karangillsiementrainingbucket'
const key=new Date()+"object.json"


exports.lambdaHandler = async (event, context) => {
    try {
        // const ret = await axios(url);
        console.log("\n\n\n\n----"+event.body)
        if(isJson(event.body)){
            console.log("\n\n\n\n\nokkkkkkk------\n\n\n\n\n")
            await S3.upload({
                Body: event.body,
                Bucket: Bucket,
                ContentType: 'application/json',
                Key: key
            }).promise().then(
                response = {
                    'statusCode': 200,
                    'body': JSON.stringify({
                        message: "Ok",
                        // location: ret.data.trim()
                    })
                }
            ).catch(err=>{
                console.log("\n\n\nkikikiki"+err)
            })
            
            
        }
        else{
            response = {
                'statusCode': 412,
                'body': JSON.stringify({
                    message: "Json not included in the body"
                })
            }
        }

      
    } catch (err) {
        console.log(err);
        return err;
    }

    return response
};

function isJson(item) {
    item = typeof item !== "string"
        ? JSON.stringify(item)
        : item;

    try {
        item = JSON.parse(item);
    } catch (e) {
        return false;
    }

    if (typeof item === "object" && item !== null) {
        return true;
    }

    return false;
}